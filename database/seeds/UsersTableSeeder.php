<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'first_name' => 'admin',
            'last_name' => 'admin',
            'gender' => 'male',
            'birthdate' => '1991-01-01',
            'mobile_number' => '09274282109',
            'role' => 1,
            'email' => 'admin@demo.com',
        ]);
    }
}
