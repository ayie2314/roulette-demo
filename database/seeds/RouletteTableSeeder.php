<?php

use Illuminate\Database\Seeder;

class RouletteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roulettes')->insert([
            [
                'name' => 'Roulette',
                'title' => 'A'
            ],
            [
                'name' => 'Roulette',
                'title' => 'B'
            ],
            [
                'name' => 'Roulette',
                'title' => 'C'
            ],
            [
                'name' => 'Roulette',
                'title' => 'D'
            ]
        ]);
    }
}
