<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth','prefix' => 'roulette'],function () {
    Route::get('/', 'RouletteController@index');
    Route::post('/update', 'RouletteController@update');
});
Route::group(['middleware' => 'auth','prefix' => 'admin-list'],function () {
    Route::get('/', 'UserController@index');
});
Route::group(['middleware' => 'auth','prefix' => 'create-admin'],function () {
    Route::get('/', 'UserController@create');
    Route::post('/', 'UserController@store');
});
Route::group(['middleware' => 'auth','prefix' => 'users'],function () {
    Route::get('/', 'UserController@index');
    Route::post('/', 'UserController@update');
});

Route::group(['middleware' => 'auth','prefix' => 'bet'],function () {
    Route::get('/', 'HomeController@bet');
    Route::post('/', 'HomeController@place_bet');
});
