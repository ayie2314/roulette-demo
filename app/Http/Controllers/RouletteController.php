<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roulette;
use DB;

class RouletteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roulettes = Roulette::all();
        return view('admin.roulette',compact('roulettes'));
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $to_update = [];
            foreach($request->except('_token') as $key=>$value) {
                $key = explode('-',$key);
                $to_update[$key[1]][$key[0]] = $value;   
            }
            foreach($to_update as $id => $data) {
                Roulette::find($id)->update($data);
            }
            DB::commit();
            return redirect()->back()->with('success', 'Roulettes successfully updated');   
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error', 'Something went wrong');   
        }
       
    }
}
