<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Roulette;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::where('role','!=',1)->paginate(5);
        return view('home',compact('users'));
    }

    public function bet()
    {
        $roulettes = Roulette::all();
        return view('user.bet',compact('roulettes'));
    }

    public function place_bet(Request $request)
    {   
        if($request->amount >= 1) {
            if(Auth::user()->balance >= $request->amount) {
                $user = Auth::user();
                $user->balance = $user->balance - $request->amount;
                $user->bet = $user->bet + $request->amount;
                $user->save();
                return redirect()->back()->with('bet_success-'.$request->roulette_id, 'Bet successfully placed'); 
            }else{
                return redirect()->back()->with('bet_error-'.$request->roulette_id, 'Insufficient funds'); 
            }
        }else{
            return redirect()->back()->with('bet_error-'.$request->roulette_id, 'Invalid amount'); 
        }
    }
}
