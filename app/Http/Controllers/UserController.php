<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::where('role','=',1)->paginate(5);
        return view('admin.admin-list',compact('users'));
    }

    public function update(Request $request) {
        foreach($request->except('_token') as $key=>$value) {
            $key = explode('-',$key);
            if($key[0] == 'button') {
                User::find($key[1])->update([
                    'balance' => $request['amount-'.$key[1]],
                    'bet' => $request['bet-'.$key[1]]
                ]);
                return redirect()->back()->with('success', 'User successfully updated');   
            }   
        }

    }

    public function create() {
        return view('admin.add-admin');
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'string', 'max:255'],
            'birthdate' => ['required', 'string', 'max:255'],
            'mobile_number' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return redirect('/create-admin')
                        ->withErrors($validator)
                        ->withInput();
        }

        User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'gender' => $request->gender,
            'birthdate' => $request->birthdate,
            'mobile_number' => $request->mobile_number,
            'role' => 1,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return redirect('/admin-list')->with('success', 'Admin successfully created');   
    }
}
