@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Bet on roulette</div> 
                <div class="card-body">
                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{!! \Session::get('success') !!}</li>
                            </ul>
                        </div>
                    @endif
                        <div class="row justify-content-center">
                            @foreach($roulettes as $roulette)
                                <div class="col-md-5" style="margin-bottom : 10px;">
                                    @if (\Session::has('bet_success-'.$roulette->id))
                                        <div class="alert alert-success">
                                            <ul>
                                                <li>{!! \Session::get('bet_success-'.$roulette->id) !!}</li>
                                            </ul>
                                        </div>
                                    @endif 
                                    @if (\Session::has('bet_error-'.$roulette->id))
                                        <div class="alert alert-danger">
                                            <ul>
                                                <li>{!! \Session::get('bet_error-'.$roulette->id) !!}</li>
                                            </ul>
                                        </div>
                                    @endif 
                                    <div class="card">
                                        <div class="card-header">{{ $roulette->name }}</div> 
                                        <div class="card-body">
                                            {{ $roulette->title }}
                                        </div>
                                    </div>
                                    <br>
                                    <form method="POST" action="/bet">
                                        @csrf
                                        <input type="hidden" name="roulette_id" value="{{ $roulette->id }}">
                                        <input type="text" class="form-control" name="amount" value="0">
                                        <br>
                                        <button type="submit" class="btn btn-primary">Bet</button>
                                    </form>
                                </div>
                                <br>
                                <br>
                            @endforeach
                        </div> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
