@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Roulette Maintenance</div> 
                <div class="card-body">
                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{!! \Session::get('success') !!}</li>
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="/roulette/update">
                        @csrf

                        <div class="row justify-content-center">
                            @foreach($roulettes as $roulette)
                            <div class="col-md-5" style="margin-bottom : 10px;">
                                <div class="card">
                                    <div class="card-header"><input type="text" class="form-control" name="name-{{ $roulette->id }}" value="{{ $roulette->name }}"></div> 
                                    <div class="card-body">
                                        <input type="text" class="form-control" name="title-{{ $roulette->id }}" value="{{ $roulette->title }}">
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div> 
                        <br>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
