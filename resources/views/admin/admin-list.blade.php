@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{!! \Session::get('success') !!}</li>
                            </ul>
                        </div>
                    @endif

                    <div class="container" style="overflow-x : auto">
                        <h3>Registered Admins <a class="btn btn-primary btn-sm" href="/create-admin"> Add Admin</a></h3>
                        <form method="POST" action="/users">
                            @csrf
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td>Name</td>
                                        <td>Mobile Number</td>
                                        <td>Birthday</td>
                                        <td>Gender</td>
                                        <td>Email</td>
                                        <td>Created at</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                                <td>
                                                    {{ $user->full_name }}
                                                </td>
                                                <td>{{ $user->mobile_number }}</td>
                                                <td>{{ $user->birthdate }}</td>
                                                <td>{{ $user->gender }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>{{ $user->created_at }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </form>
                        {{ $users->links() }}
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
