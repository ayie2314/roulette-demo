@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{!! \Session::get('success') !!}</li>
                            </ul>
                        </div>
                    @endif

                    <div class="container" style="overflow-x : auto">
                    @if(Auth::user()->role == 1)
                        <h3>Registerd Users</h3>
                        <form method="POST" action="/user">
                            @csrf
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td>Name</td>
                                        <td>Mobile Number</td>
                                        <td>Birthday</td>
                                        <td>Gender</td>
                                        <td>Email</td>
                                        <td>Bet</td>
                                        <td>Balance</td>
                                        <td>Created at</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                        
                                                <td>
                                                    {{ $user->full_name }}
                                                </td>
                                                <td>{{ $user->mobile_number }}</td>
                                                <td>{{ $user->birthdate }}</td>
                                                <td>{{ $user->gender }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>
                                                    <input type="text" name="bet-{{$user->id}}" value="{{ $user->bet }}">
                                                </td>
                                                <td>
                                                    <input type="text" name="amount-{{$user->id}}" value="{{ $user->balance }}">
                                                </td>
                                                <td>{{ $user->created_at }}</td>
                                                <td>
                                                    <button type="submit" name="button-{{$user->id}}" class="btn btn-primary btn-sm">Update</button>
                                                </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </form>
                        {{ $users->links() }}
                    @else
                        <p>Name : {{ Auth::user()->full_name }}</p>
                        <p>Mobile Number : {{ Auth::user()->mobile_number }}</p>
                        <p>Birthday : {{ Auth::user()->birthdate }}</p>
                        <p>Gender : {{ Auth::user()->gender }}</p>
                        <p>Email : {{ Auth::user()->email }}</p>
                        <p>Balance : {{ Auth::user()->balance }}</p>
                        <p>Created at : {{ Auth::user()->created_at }}</p>
                        <a href="/bet" class="btn btn-primary">Start to bet</a>
                    @endif
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
